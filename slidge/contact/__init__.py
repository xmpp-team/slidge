"""
Everything related to 1 on 1 chats, and other legacy users' details.
"""

from .contact import LegacyContact
from .roster import LegacyRoster

__all__ = ("LegacyContact", "LegacyRoster")
