========
For devs
========

.. toctree::
   :maxdepth: 2

   contributing
   design
   tutorial
   howto
   api/slidge/index
   api/superduper/index
