.. note::

  For legacy module-specific options, refer to their own docs:
  `matridge <https://slidge.im/docs/matridge/main/config.html>`_,
  `matteridge <https://slidge.im/docs/matteridge/main/config.html>`_,
  `messlidger <https://slidge.im/docs/messlidger/main/config.html>`_,
  `skidge <https://slidge.im/docs/skidge/main/config.html>`_,
  `sleamdge <https://slidge.im/docs/sleamdge/main/config.html>`_,
  `slidcord <https://slidge.im/docs/slidcord/main/config.html>`_,
  `slidge-whatsapp <https://slidge.im/docs/slidge-whatsapp/main/config.html>`_,
  `slidgnal <https://slidge.im/docs/slidgnal/main/config.html>`_,
  `slidgram <https://slidge.im/docs/slidgram/main/config.html>`_.
