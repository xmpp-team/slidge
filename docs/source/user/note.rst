.. note::

  These are the generic user docs for slidge. For
  :term:`Legacy Network`-specific docs, follow these links:
  `matridge <https://slidge.im/docs/matridge/main/user.html>`_,
  `matteridge <https://slidge.im/docs/matteridge/main/user.html>`_,
  `messlidger <https://slidge.im/docs/messlidger/main/user.html>`_,
  `skidge <https://slidge.im/docs/skidge/main/user.html>`_,
  `sleamdge <https://slidge.im/docs/sleamdge/main/user.html>`_,
  `slidcord <https://slidge.im/docs/slidcord/main/user.html>`_,
  `slidge-whatsapp <https://slidge.im/docs/slidge-whatsapp/main/user.html>`_,
  `slidgnal <https://slidge.im/docs/slidgnal/main/user.html>`_,
  `slidgram <https://slidge.im/docs/slidgram/main/user.html>`_.
